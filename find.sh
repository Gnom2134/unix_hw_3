#!/usr/bin/env bash

find . -mtime -2 -mtime +1 -type f ! -name "*.sh" -print0 |
	while IFS= read -r -d '' line
	do
		export line
		mv "$line" "$(dirname $line)/_$(basename $line)"
	done	
