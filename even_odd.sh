#!/usr/bin/env bash

read -p "Print your number " x
export x
x=$(echo -n "$x" | tail -c 1)
export x
if [ $(( "$x" % 2 )) -eq 0 ]
then
	echo "EVEN"
else
	echo "ODD"
fi
