#!/usr/bin/env bash

IFS=''
read -r -p "Enter your string: " x
export x
if [ $(echo -n "$x" | rev) = "$x" ]
then
	echo "YES"
else
	echo "NO"
fi	
