#!/usr/bin/env bash

echo -n  "" > $2
while IFS= read -r line
do
	n="$line"
	factor "$n" >> $2
done < "$1"
