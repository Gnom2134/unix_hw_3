#!/usr/bin/env bash

while IFS= read -r line
do
	IFS=
	test=$(ping -c 1 -i 1 -W 1 $line)
	if [ "$?" -eq 0 ]
	then
		echo -e $test >> res.txt
	else
		echo -e $test >> err.txt
	fi
	echo $line	
done < ips.txt
